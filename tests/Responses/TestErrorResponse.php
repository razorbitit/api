<?php declare(strict_types=1);

namespace RazorBit\API\Tests\Responses;

use PHPUnit\Framework\TestCase;

use RazorBit\API\Responses\ErrorResponse;

class TestErrorResponse extends TestCase
{
    // Test data
    private $expectedStatusCode         = 500;
    private $defaultKey                 = 'message';
    private $defaultValue               = 'Internal Server Error';

    // Test class
    private $errorResponse;

    public function setUp()
    {
        // Setup test class
        $this->errorResponse            = new ErrorResponse();
    }

    /**
     * Check if getStatusCode returns 500
     *
     * @return void
     */
    public function testGetStatusCodeReturnsCorrectStatusCode()
    {
        $statusCode                     = $this->errorResponse->getStatusCode();

        $this->assertEquals($this->expectedStatusCode, $statusCode);
    }
    
    /**
     * Check if display echoes array with default message
     *
     * @return void
     */
    public function testDisplayEchoesDefaultMessage()
    {
        $result                         = $this->getArrayFromDisplay($this->errorResponse);

        $this->assertEquals($result[$this->defaultKey], $this->defaultValue);
    }

    /**
     * Turn data from display into json array
     *
     * @param ErrorResponse $error
     *
     * @return array
     */
    private function getArrayFromDisplay(ErrorResponse $error): array
    {
        ob_start();
        $error->display();
        $result = ob_get_contents();
        ob_end_clean();

        return json_decode($result, true);
    }
}
