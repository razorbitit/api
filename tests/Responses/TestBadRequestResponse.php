<?php declare(strict_types=1);

namespace RazorBit\API\Tests\Responses;

use PHPUnit\Framework\TestCase;

use RazorBit\API\Responses\BadRequestResponse;

class TestBadRequestResponse extends TestCase
{
    // Test data
    private $expectedStatusCode         = 400;
    private $fieldKey                   = 'KEY';
    private $fieldValue                 = 'VALUE';
    private $defaultKey                 = 'message';
    private $defaultValue               = 'Bad Request';
    private $invalidData                = array();

    public function setUp()
    {
        // Setup test data
        $this->invalidData = [
            $this->fieldKey => $this->fieldValue
        ];
    }

    /**
     * Check if display echoes invalid fields
     * given through constructor
     * 
     * @return void
     */
    public function testDisplayEchoesInvalidFieldsFromConstructor()
    {
        $badRequestResponse         = new BadRequestResponse($this->invalidData);

        $result                     = $this->getArrayFromDisplay($badRequestResponse);

        $this->assertEquals($result[$this->fieldKey], $this->fieldValue);
    }

    /**
     * Check if display echoes default message
     * when no invalid fields are passed through
     * the constructor
     * 
     * @return void
     */
    public function testDisplayEchoesDefaultMessageForEmptyConstructor()
    {
        $badRequestResponse         = new BadRequestResponse();

        $result                     = $this->getArrayFromDisplay($badRequestResponse);

        $this->assertEquals($result[$this->defaultKey], $this->defaultValue);
    }

    /**
     * Check if display echoes default message
     * when null is passed through the constructor
     * 
     * @return void
     */
    public function testDisplayEchoesDefaultMessageForNullConstructor()
    {
        $badRequestResponse         = new BadRequestResponse(null);

        $result                     = $this->getArrayFromDisplay($badRequestResponse);

        $this->assertEquals($result[$this->defaultKey], $this->defaultValue);
    }

    /**
     * Check if display echoes invalid fields
     * given through method
     * 
     * @return void
     */
    public function testDisplayEchoesInvalidFieldsFromMethod()
    {
        $badRequestResponse         = new BadRequestResponse();
        $badRequestResponse->addInvalidFields($this->invalidData);

        $result                     = $this->getArrayFromDisplay($badRequestResponse);

        $this->assertEquals($result[$this->fieldKey], $this->fieldValue);
    }

    /**
     * Check if getStatusCode returns 401
     *
     * @return void
     */
    public function testGetStatusCodeReturnsCorrectStatusCode()
    {
        $badRequestResponse = new BadRequestResponse();

        $statusCode = $badRequestResponse->getStatusCode();

        $this->assertEquals($this->expectedStatusCode, $statusCode);
    }

    /**
     * Turn data from display into json array
     *
     * @param BadRequestResponse $badRequest
     *
     * @return array
     */
    private function getArrayFromDisplay(BadRequestResponse $badRequest): array
    {
        ob_start();
        $badRequest->display();
        $result = ob_get_contents();
        ob_end_clean();

        return json_decode($result, true);
    }
}
