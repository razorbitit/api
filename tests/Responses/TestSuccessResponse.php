<?php declare(strict_types=1);

namespace RazorBit\API\Tests\Responses;

use PHPUnit\Framework\TestCase;

use RazorBit\API\Contracts\IModel;
use RazorBit\API\Responses\SuccessResponse;

class TestSuccessResponse extends TestCase
{
    // Test data
    private $expectedStatusCode         = 200;
    private $fieldKey                   = 'KEY';
    private $fieldValue                 = 'VALUE';
    private $defaultKey                 = 'message';
    private $defaultValue               = 'Success';
    private $data                       = array();

    // Mocks
    private $mockModel;

    public function setUp()
    {
        // Setup test data
        $this->data                     = [
            $this->fieldKey     => $this->fieldValue
        ];

        // Setup mocks
        $this->mockModel                = $this->createMock(IModel::class);
    }

    /**
     * Check if display echoes the model
     * given through the constructor
     * 
     * @return void
     */
    public function testDisplayEchoesModelFromConstructor()
    {
        $this->mockModel->method('toArray')->willReturn($this->data);
        $successResponse                = new SuccessResponse($this->mockModel);

        $result                         = $this->getArrayFromDisplay($successResponse);

        $this->assertEquals($result, $this->data);
    }

    /**
     * Check if display echoes default message
     * when constructor is left empty
     * 
     * @return void
     */
    public function testDisplayEchoesDefaultMessageForEmptyConstructor()
    {
        $successResponse                = new SuccessResponse();

        $result                         = $this->getArrayFromDisplay($successResponse);

        $this->assertEquals($result[$this->defaultKey], $this->defaultValue);
    }

    /**
     * Check if display echoes default message
     * when a null value is given through the
     * constructor
     * 
     * @return void
     */
    public function testDisplayEchoesDefaultMessageWhenNullIsGivenThroughConstructor()
    {
        $successResponse                = new SuccessResponse(null);

        $result                         = $this->getArrayFromDisplay($successResponse);

        $this->assertEquals($result[$this->defaultKey], $this->defaultValue);
    }

    /**
     * Check if getBody returns an empty array
     * when an empty array is set
     *
     * @return void
     */
    public function testGetBodyReturnsEmptyArrayWhenArrayIsEmpty()
    {
        $emptyArray                     = array();
        $successResponse                = new SuccessResponse();
        $successResponse->setBody($emptyArray);

        $result                         = $this->getArrayFromDisplay($successResponse);

        $this->assertEquals($emptyArray, $result);
    }

    /**
     * Check if setBody is being echoed by display
     *
     * @return void
     */
    public function testSetBodyDisplaysGivenBody()
    {
        $successResponse                = new SuccessResponse();
        $successResponse->setBody($this->data);

        $result                         = $this->getArrayFromDisplay($successResponse);

        $this->assertEquals($result[$this->fieldKey], $this->fieldValue);
    }

    /**
     * Check if the status code for the response matches
     * the one we expect
     *
     * @return void
     */
    public function testGetStatusCodeReturnsCorrectStatusCode()
    {
        $successResponse                = new SuccessResponse();

        $statusCode                     = $successResponse->getStatusCode();

        $this->assertEquals($this->expectedStatusCode, $statusCode);
    }

    /**
     * Turn data from display into json array
     *
     * @param SuccessResponse $success
     *
     * @return array
     */
    private function getArrayFromDisplay(SuccessResponse $success): array
    {
        ob_start();
        $success->display();
        $result = ob_get_contents();
        ob_end_clean();

        return json_decode($result, true);
    }
}
