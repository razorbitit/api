<?php declare(strict_types=1);

namespace RazorBit\API\Tests\Responses;

use PHPUnit\Framework\TestCase;

use RazorBit\API\Responses\UnauthorizedResponse;

class TestUnauthorizedResponse extends TestCase
{
    // Test data
    private $expectedStatusCode         = 401;
    private $defaultKey                 = 'message';
    private $defaultValue               = 'Unauthorized';

    // Test class
    private $unauthorizedResponse;

    public function setUp()
    {
        // Setup test class
        $this->unauthorizedResponse     = new UnauthorizedResponse();
    }

    /**
     * Check if getStatusCode returns 401
     * 
     * @return void
     */
    public function testGetStatusCodeReturnsCorrectStatusCode()
    {
        $statusCode                     = $this->unauthorizedResponse->getStatusCode();

        $this->assertEquals($this->expectedStatusCode, $statusCode);
    }

    /**
     * Check if display echoes default message
     * 
     * @return void
     */
    public function testDisplayEchoesDefaultMessage()
    {
        $result                         = $this->getArrayFromDisplay($this->unauthorizedResponse);

        $this->assertEquals($result[$this->defaultKey], $this->defaultValue);
    }

    /**
     * Turn data from display into json array
     *
     * @param UnauthorizedResponse $unauthorized
     *
     * @return array
     */
    private function getArrayFromDisplay(UnauthorizedResponse $unauthorized): array
    {
        ob_start();
        $unauthorized->display();
        $result = ob_get_contents();
        ob_end_clean();

        return json_decode($result, true);
    }
}
