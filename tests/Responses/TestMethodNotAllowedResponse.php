<?php declare(strict_types=1);

namespace RazorBit\API\Tests\Responses;

use PHPUnit\Framework\TestCase;

use RazorBit\API\Responses\MethodNotAllowedResponse;

class TestMethodNotAllowedResponse extends TestCase
{
    // Test data
    private $expectedStatusCode             = 405;
    private $defaultKey                     = 'message';
    private $defaultValue                   = 'Method Not Allowed';

    // Test class
    private $methodNotAllowedResponse;

    public function setUp()
    {
        // Setup test class
        $this->methodNotAllowedResponse     = new MethodNotAllowedResponse();
    }

    /**
     * Check if getStatusCode returns 405
     *
     * @return void
     */
    public function testGetStatusCodeReturnsCorrectStatusCode()
    {
        $statusCode                         = $this->methodNotAllowedResponse->getStatusCode();

        $this->assertEquals($this->expectedStatusCode, $statusCode);
    }

    /**
     * Check if display echoes default message
     * 
     * @return void
     */
    public function testGetDisplayEchoesDefaultMessage()
    {
        $result                             = $this->getArrayFromDisplay($this->methodNotAllowedResponse);

        $this->assertEquals($result[$this->defaultKey], $this->defaultValue);
    }

    /**
     * Turn data from display into json array
     *
     * @param MethodNotAllowedResponse $methodNotAllowed
     *
     * @return array
     */
    private function getArrayFromDisplay(MethodNotAllowedResponse $methodNotAllowed): array
    {
        ob_start();
        $methodNotAllowed->display();
        $result = ob_get_contents();
        ob_end_clean();

        return json_decode($result, true);
    }
}
