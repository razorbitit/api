<?php declare(strict_types=1);

namespace RazorBit\API\Tests\Responses;

use PHPUnit\Framework\TestCase;

use RazorBit\API\Responses\NotFoundResponse;

class TestNotFoundResponse extends TestCase
{
    // Test data
    private $expectedStatusCode         = 404;
    private $defaultKey                 = 'message';
    private $defaultValue               = 'Not Found';

    // Test class
    private $notFoundResponse;

    public function setUp()
    {
        // Setup test class
        $this->notFoundResponse         = new NotFoundResponse();
    }

    /**
     * Check if getStatusCode returns 404
     *
     * @return void
     */
    public function testGetStatusCodeReturnsCorrectStatusCode()
    {
        $statusCode                     = $this->notFoundResponse->getStatusCode();

        $this->assertEquals($this->expectedStatusCode, $statusCode);
    }

    /**
     * Check if display echoes default message
     * 
     * @return void
     */
    public function testDisplayEchoesDefaultMessage()
    {
        $result                         = $this->getArrayFromDisplay($this->notFoundResponse);

        $this->assertEquals($result[$this->defaultKey], $this->defaultValue);
    }

    /**
     * Turn data from display into json array
     *
     * @param NotFoundResponse $notFound
     *
     * @return array
     */
    private function getArrayFromDisplay(NotFoundResponse $notFound): array
    {
        ob_start();
        $notFound->display();
        $result = ob_get_contents();
        ob_end_clean();

        return json_decode($result, true);
    }
}
