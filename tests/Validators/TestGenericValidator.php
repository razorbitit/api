<?php declare(strict_types=1);

namespace RazorBit\API\Tests\Validators;

use PHPUnit\Framework\TestCase;

use RazorBit\API\Validators\GenericValidator;

class TestGenericValidator extends TestCase
{
    use GenericValidator;

    // Test data
    private $validEmailAddress          = 'myemail@hosting.com';
    private $invalidEmailAddress        = 'non_existing.tests@localhost@test.com';

    private $validPhoneNumber           = '06123456789';
    private $validPhoneNumberSpaces     = '06 1234 5678 9';
    private $validPhoneNumberAreaCode   = '+316 1234 5678 9';
    private $invalidPhoneNumber         = '00000000a';
    private $invalidPhoneNumberSpaces   = '00 0000 0000 a';
    private $invalidPhoneNumberAreaCode = '+310 0000 0000 a';

    private $stringA                    = 'this is my own string';
    private $stringB                    = 'this is my other string';
    private $nullValue                  = null;
    private $integerAsString            = '23546';
    private $double                     = 4.5;
    private $smallInteger               = 1;
    private $bigInteger                 = 9944938273347346;

    /**
     * Check if validateEmailAddress returns true
     * for valid email address
     *
     * @return void
     */
    public function testValidateEmailAddressReturnsTrueForValidEmailAddress()
    {
        $result                 = $this->validateEmailAddress($this->validEmailAddress);

        $this->assertTrue($result);
    }

    /**
     * Check if validateEmailAddress returns false
     * for invalid email address
     *
     * @return void
     */
    public function testValidateEmailAddressReturnsFalseForInvalidEmailAddress()
    {
        $result                 = $this->validateEmailAddress($this->invalidEmailAddress);

        $this->assertFalse($result);
    }

    /**
     * Check if validatePhoneNumber returns true
     * for valid phone number
     *
     * @return void
     */
    public function testValidatePhoneNumberReturnsTrueForValidPhoneNumber()
    {
        $result                 = $this->validatePhoneNumber($this->validPhoneNumber);

        $this->assertTrue($result);
    }

    /**
     * Check if validatePhoneNumber returns true
     * for valid phone number with spaces
     *
     * @return void
     */
    public function testValidatePhoneNumberReturnsTrueForValidPhoneNumberWithSpaces()
    {
        $result                 = $this->validatePhoneNumber($this->validPhoneNumberSpaces);

        $this->assertTrue($result);
    }

    /**
     * Check if validatePhoneNumber returns true
     * for valid phone number with area code
     *
     * @return void
     */
    public function testValidatePhoneNumberReturnsTrueForValidPhoneNumberWithAreaCode()
    {
        $result                 = $this->validatePhoneNumber($this->validPhoneNumberAreaCode);

        $this->assertTrue($result);
    }

    /**
     * Check if validatePhoneNumber returns false
     * for invalid phone number
     *
     * @return void
     */
    public function testValidatePhoneNumberReturnsFalseForInvalidPhoneNumber()
    {
        $result                 = $this->validatePhoneNumber($this->invalidPhoneNumber);

        $this->assertFalse($result);
    }

    /**
     * Check if validatePhoneNumber returns false
     * for invalid phone number with spaces
     *
     * @return void
     */
    public function testValidatePhoneNumberReturnsFalseForInvalidPhoneNumberWithSpaces()
    {
        $result                 = $this->validatePhoneNumber($this->invalidPhoneNumberSpaces);

        $this->assertFalse($result);
    }

    /**
     * Check if validatePhoneNumber returns false
     * for invalid phone number with area code
     *
     * @return void
     */
    public function testValidatePhoneNumberReturnsFalseForInvalidPhoneNumberWithAreaCode()
    {
        $result                 = $this->validatePhoneNumber($this->invalidPhoneNumberAreaCode);

        $this->assertFalse($result);
    }

    /**
     * Check if validateEquals returns true
     * for same strings
     *
     * @return void
     */
    public function testValidateEqualsReturnsTrueForSameStrings()
    {
        $result                 = $this->validateEquals($this->stringA, $this->stringA);

        $this->assertTrue($result);
    }

    /**
     * Check if validateEquals returns false
     * for different strings
     *
     * @return void
     */
    public function testValidateEqualsReturnsFalseForDifferentStrings()
    {
        $result                 = $this->validateEquals($this->stringA, $this->stringB);

        $this->assertFalse($result);
    }

    /**
     * Check if validateMinLength returns true
     * for string that's larger than the given minLength
     *
     * @return void
     */
    public function testValidateMinLengthReturnsTrueForValueLargerThanMinLength()
    {
        $lessLength             = strlen($this->stringA) - 5;
        $result                 = $this->validateMinLength($this->stringA, $lessLength);

        $this->assertTrue($result);
    }

    /**
     * Check if validateMinLength returns true
     * for string that's equal to the given minLength
     *
     * @return void
     */
    public function testValidateMinLengthReturnsTrueForValueEqualToMinLength()
    {
        $sameLength             = strlen($this->stringA);
        $result                 = $this->validateMinLength($this->stringA, $sameLength);

        $this->assertTrue($result);
    }

    /**
     * Check if validateMinLength return false
     * for string that's smaller than the given minLength
     *
     * @return void
     */
    public function testValidateMinLengthReturnsFalseForValueLessThanMinLength()
    {
        $largerLength           = strlen($this->stringA) + 5;
        $result                 = $this->validateMinLength($this->stringA, $largerLength);

        $this->assertFalse($result);
    }

    /**
     * Check if validateMinLength returns false
     * for null value
     * 
     * @return void
     */
    public function testValidateMinLengthReturnsFalseForNullValue()
    {
        $result                 = $this->validateMinLength($this->nullValue, 0);

        $this->assertFalse($result);
    }
    
    /**
     * Check if validateMaxLength returns true
     * for string that's smaller than the given maxLength
     *
     * @return void
     */
    public function testValidateMaxLengthReturnsTrueForValueLessThanMaxLength()
    {
        $largerLength           = strlen($this->stringA) + 5;
        $result                 = $this->validateMaxLength($this->stringA, $largerLength);

        $this->assertTrue($result);
    }

    /**
     * Check if validateMaxLength returns true
     * for string that's equal to the given maxLength
     *
     * @return void
     */
    public function testValidateMaxLengthReturnsTrueForValueEqualToMaxLength()
    {
        $equalLength            = strlen($this->stringA);
        $result                 = $this->validateMaxLength($this->stringA, $equalLength);

        $this->assertTrue($result);
    }

    /**
     * Check if validateMaxLength returns false
     * for string that's larger the given maxLength
     *
     * @return void
     */
    public function testValidateMaxLengthReturnsFalseForValueLargerThanMaxLength()
    {
        $lessLength             = strlen($this->stringA) - 5;
        $result                 = $this->validateMaxLength($this->stringA, $lessLength);

        $this->assertFalse($result);
    }

    /**
     * Check if validateMaxLength returns false
     * when the value is null
     * 
     * @return void
     */
    public function testValidateMaxLengthReturnsFalseForNullValue()
    {
        $result                 = $this->validateMaxLength($this->nullValue, 0);

        $this->assertFalse($result);
    }

    /**
     * Check if validateInteger returns true
     * for small and big integers
     * 
     * @return void
     */
    public function testValidateIntegerReturnsTrueForValidInteger()
    {
        $resultSmallInteger     = $this->validateInteger($this->smallInteger);
        $resultBigInteger       = $this->validateInteger($this->bigInteger);

        $this->assertTrue($resultSmallInteger);
        $this->assertTrue($resultBigInteger);
    }

    /**
     * Check if validateInteger returns true
     * for an integer disguised as string
     * 
     * i.e.: "1" or "25"
     * 
     * @return void
     */
    public function testValidateIntegerReturnsTrueForIntegerAsString()
    {
        $result                 = $this->validateInteger($this->integerAsString);

        $this->assertTrue($result);
    }

    /**
     * Check if validateInteger returns false
     * for a double
     * 
     * i.e.: 3.14
     * 
     * @return void
     */
    public function testValidateIntegerReturnsFalseForDouble()
    {
        $result                 = $this->validateInteger($this->double);

        $this->assertFalse($result);
    }

    /**
     * Check if validateInteger returns false
     * for a string
     * 
     * @return void
     */
    public function testValidateIntegerReturnsFalseForString()
    {
        $result                 = $this->validateInteger($this->stringA);

        $this->assertFalse($result);
    }

    /**
     * Check if validateDateForamt returns true
     * when the date matches the given format
     * 
     * @return void
     */
    public function testValidateDateFormatReturnsTrueForValidDateFormat()
    {
        $dateFormat             = 'Y-m-d';
        $date1                  = '2045-12-31';
        $date2                  = '1995-11-12';

        $resultDate1            = $this->validateDateFormat($date1, $dateFormat);
        $resultDate2            = $this->validateDateFormat($date2, $dateFormat);

        $this->assertTrue($resultDate1);
        $this->assertTrue($resultDate2);
    }

    /**
     * Check if validateDateFormat returns true
     * when the date matches the given format when
     * the format has a constant in it
     * 
     * @return void
     */
    public function testValidateDateFormatReturnsTrueForValidDateFormatWithConstant()
    {
        $dateFormat             = 'Y-m-d\TH:i:s';
        $date1                  = '2012-12-23T23:11:43';
        $date2                  = '2034-04-14T11:34:59';

        $resultDate1            = $this->validateDateFormat($date1, $dateFormat);
        $resultDate2            = $this->validateDateFormat($date2, $dateFormat);

        $this->assertTrue($resultDate1);
        $this->assertTrue($resultDate2);
    }

    /**
     * Check if validateDateFormat returns false
     * when the date doesn't match the given format
     * 
     * @return void
     */
    public function testValidateDateFormatReturnsFalseForInvalidDateFormat()
    {
        $dateFormat             = 'm-d H:i';
        $date1                  = '2019-11-12 15:04';
        $date2                  = '31-12 59:23';

        $resultDate1            = $this->validateDateFormat($date1, $dateFormat);
        $resultDate2            = $this->validateDateFormat($date2, $dateFormat);

        $this->assertFalse($resultDate1);
        $this->assertFalse($resultDate2);
    }

    /**
     * Check if validateNonNull returns true
     * for integer
     * 
     * @return void
     */
    public function testValidateNonNullReturnsTrueForInteger()
    {
        $resultSmallInteger         = $this->validateNonNull($this->smallInteger);
        $resultBigInteger           = $this->validateNonNull($this->bigInteger);
        $resultZeroInteger          = $this->validateNonNull(0);

        $this->assertTrue($resultSmallInteger);
        $this->assertTrue($resultBigInteger);
        $this->assertTrue($resultZeroInteger);
    }

    /**
     * Check if validateNonNull returns true
     * for string
     */
    public function testValidateNonNullReturnsTrueForString()
    {
        $result                     = $this->validateNonNull($this->stringA);

        $this->assertTrue($result);
    }

    /**
     * Check if validateNonNull returns false
     * for null value
     */
    public function testValidateNonNullReturnsFalseForNullValue()
    {
        $result                     = $this->validateNonNull($this->nullValue);

        $this->assertFalse($result);
    }
}
