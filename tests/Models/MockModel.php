<?php declare(strict_types=1);

namespace RazorBit\API\Tests\Models;

use RazorBit\API\Models\BaseModel;

class MockModel extends BaseModel
{
    public $id;
    public $username;
    public $password;
    public $child;

    protected $hiddenFields = [
        'password'
    ];
}
