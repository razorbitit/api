<?php declare(strict_types=1);

namespace RazorBit\API\Tests\Models;

use PHPUnit\Framework\TestCase;

use RazorBit\API\Tests\Models\MockModel;

class TestBaseModel extends TestCase
{
    // Test data
    private $id             = 1;
    private $username       = 'user';
    private $password       = 'pass';
    private $hiddenField    = 'password';

    // Test class
    private $mockModel;

    public function setUp()
    {
        // Setup test class
        $this->mockModel            = new MockModel();

        $this->mockModel->id        = $this->id;
        $this->mockModel->username  = $this->username;
        $this->mockModel->password  = $this->password;
        $this->mockModel->child     = new MockModel();
    }

    /**
     * Check if toArray method doesn't return hiddenFields member
     *
     * @return void
     */
    public function testIfToArrayHidesHiddenFieldsMember()
    {
        $result         = $this->mockModel->toArray();

        $this->assertArrayNotHasKey('hiddenFields', $result);
    }

    /**
     * Check if toArray returns visible fields
     *
     * @return void
     */
    public function testIfToArrayReturnsCorrectData()
    {
        $result         = $this->mockModel->toArray();

        $this->assertEquals($result['id']       , $this->id);
        $this->assertEquals($result['username'] , $this->username);
    }

    /**
     * Check if toArray doesn't return hidden fields
     *
     * @return void
     */
    public function testIfToArrayHidesHiddenFields()
    {
        $result         = $this->mockModel->toArray();

        $this->assertArrayNotHasKey($this->hiddenField, $result);
    }

    /**
     * Check if toArray doesn't return members hidden fields
     * 
     * This assumes that one of the children is an IModel
     * implementation
     *
     * @return void
     */
    public function testToArrayDisplaysPublicFieldsForMemberModels()
    {
        $result         = $this->mockModel->toArray();
        $childArray     = $result['child'];

        $this->assertArrayNotHasKey('hiddenFields'  , $childArray);
        $this->assertArrayNotHasKey('password'      , $childArray);
    }
}
