<?php declare(strict_types=1);

namespace RazorBit\API\Tests\Requests;

use PHPUnit\Framework\TestCase;

use RazorBit\API\Requests\PutRequest;

/**
 * Cannot test wheter the expected values can be fetched
 * from a PUT request 'cause the php://input file cannot
 * be written to in these tests
 *
 */
class TestPutRequest extends TestCase
{
    // Test data
    private $requestKey             = 'KEY';
    private $requestData            = 'VALUE';
    private $integerKey             = 'INT_VAL';
    private $integerValue           = '12';
    private $defaultData            = 'DEF_VALUE';
    private $defaultIntegerValue    = 0;
    private $nonExistingHeader      = 'NON_EXISTINGHEADER';

    // Test class
    private $putRequest;

    public function setUp()
    {
        // Setup test class
        $this->putRequest           = new PutRequest();
    }

    /**
     * Check if getValue returns default value
     * when request doesn't have given key
     *
     * @return void
     */
    public function testGetValueReturnsDefaultValueWhenRequestIsEmpty()
    {
        unset($_GET[$this->requestKey]);

        $result                     = $this->putRequest->getValue($this->requestKey, $this->defaultData);

        $this->assertEquals($this->defaultData, $result);
    }

    /**
     * Check if getValue returns empty string
     * when request doesn't have given key and
     * default value is null
     *
     * @return void
     */
    public function testGetValueReturnsEmptyStringWhenRequestIsEmptyAndDefaultValueIsNull()
    {
        unset($_GET[$this->requestKey]);

        $result                     = $this->putRequest->getValue($this->requestKey, null);

        $this->assertNotNull($result);
        $this->assertEmpty($result);
    }

    /**
     * Check if getIntegerValue returns the default value
     * when the value isn't an integer and the default value
     * is set
     *
     * @return void
     */
    public function testGetIntegerValueReturnsDefaultValueWhenTheValueIsntAnInteger()
    {
        $_GET[$this->integerKey]    = $this->requestData;

        $result                     = $this->putRequest->getIntegerValue($this->integerKey, $this->defaultIntegerValue);

        $this->assertEquals($this->defaultIntegerValue, $result);
    }

    /**
     * Check if getHeader returns empty string
     * when header doesn't have given key and
     * default value isn't given
     *
     * @return void
     */
    public function testGetHeaderReturnsDefaultValueWhenHeaderCannotBeFoundAndNoDefaultValueIsSet()
    {
        $result                     = $this->putRequest->getHeader($this->nonExistingHeader);

        $this->assertEmpty($result);
    }

    /**
     * Check if getHeader returns empty string
     * when header doesn't have given key and
     * default value isn't given
     *
     * @return void
     */
    public function testGetHeaderReturnsEmptyStringWhenHeaderCannotBeFoundAndDefaultValueIsNull()
    {
        $result                     = $this->putRequest->getHeader($this->nonExistingHeader, null);

        $this->assertNotNull($result);
        $this->assertEmpty($result);
    }

    /**
     * Check if getHeader returns default value
     * when header doesn't have given key
     *
     * @return void
     */
    public function testGetHeaderReturnsGivenDataWhenHeaderCannotBeFound()
    {
        $result                     = $this->putRequest->getHeader($this->nonExistingHeader, $this->defaultData);

        $this->assertEquals($this->defaultData, $result);
    }

    /**
     * Check if the getFile returns the file array from
     * the $_FILES global when the file is available
     *
     * @return void
     */
    public function testGetFileReturnsFileArrayWhenFileCanBeFound()
    {
        $file = [
            $this->requestKey   => $this->defaultData
        ];

        $_FILES[$this->requestKey]  = $file;

        $result                     = $this->putRequest->getFile($this->requestKey);

        $this->assertEquals($file, $result);
    }

    /**
     * Check if the getFile returns an empty array
     * when the file is not available
     *
     * @return void
     */
    public function testGetFileReturnsEmptyArrayWhenFileCannotBeFound()
    {
        unset($_FILES);

        $result                     = $this->putRequest->getFile($this->requestKey);

        $this->assertEmpty($result);
    }
}
