<?php declare(strict_types=1);

namespace RazorBit\API\Tests\Requests;

use PHPUnit\Framework\TestCase;

use RazorBit\API\Requests\PostRequest;

class TestPostRequest extends TestCase
{
    // Test data
    private $requestKey             = 'KEY';
    private $requestData            = 'VALUE';
    private $integerKey             = 'INT_VAL';
    private $integerValue           = '12';
    private $defaultData            = 'DEF_VALUE';
    private $defaultIntegerValue    = 0;
    private $nonExistingHeader      = 'NON_EXISTINGHEADER';

    // Test class
    private $postRequest;

    public function setUp()
    {
        // Setup test class
        $this->postRequest          = new PostRequest();
    }

    /**
     * Check if getValue returns value from request
     *
     * @return void
     */
    public function testGetValueReturnsValueFromRequest()
    {
        $_POST[$this->requestKey] = $this->requestData;

        $result                     = $this->postRequest->getValue($this->requestKey);

        $this->assertEquals($this->requestData, $result);
    }

    /**
     * Check if getValue returns default value
     * when request doesn't have given key
     *
     * @return void
     */
    public function testGetValueReturnsDefaultValueWhenRequestIsEmpty()
    {
        unset($_POST[$this->requestKey]);

        $result                     = $this->postRequest->getValue($this->requestKey, $this->defaultData);

        $this->assertEquals($this->defaultData, $result);
    }

    /**
     * Check if getValue returns empty string
     * when request doesn't have given key and
     * default value is null
     *
     * @return void
     */
    public function testGetValueReturnsEmptyStringWhenRequestIsEmptyAndDefaultValueIsNull()
    {
        unset($_POST[$this->requestKey]);

        $result                     = $this->postRequest->getValue($this->requestKey, null);

        $this->assertNotNull($result);
        $this->assertEmpty($result);
    }

    /**
     * Check if getIntegerValue returns an integer
     * if the key is an integer
     *
     * @return void
     */
    public function testGetIntegerValueReturnsAnIntegerWhenItsAnInteger()
    {
        $_POST[$this->integerKey]    = $this->integerValue;

        $result                     = $this->postRequest->getIntegerValue($this->integerKey);

        $this->assertEquals($this->integerValue, $result);
        $this->assertTrue(is_integer($result));
    }

    /**
     * Check if getIntegerValue returns the default value
     * when the value isn't an integer and the default value
     * is set
     *
     * @return void
     */
    public function testGetIntegerValueReturnsDefaultValueWhenTheValueIsntAnInteger()
    {
        $_POST[$this->integerKey]   = $this->requestData;

        $result                     = $this->postRequest->getIntegerValue($this->integerKey, $this->defaultIntegerValue);

        $this->assertEquals($this->defaultIntegerValue, $result);
    }

    /**
     * Check if getIntegerValue returns the value of the integer
     * when it's an actual integer and a default value is given
     *
     * @return void
     */
    public function testGetIntegerReturnsAnIntegerWhenTheValueIsAnIntegerAndADefaultValueIsSet()
    {
        $_POST[$this->integerKey]   = $this->integerValue;

        $result                     = $this->postRequest->getIntegerValue($this->integerKey, $this->defaultIntegerValue);

        $this->assertEquals($this->integerValue, $result);
    }

    /**
     * Check if getHeader returns empty string
     * when header doesn't have given key and
     * default value isn't given
     *
     * @return void
     */
    public function testGetHeaderReturnsDefaultValueWhenHeaderCannotBeFoundAndNoDefaultValueIsSet()
    {
        $result                     = $this->postRequest->getHeader($this->nonExistingHeader);

        $this->assertEmpty($result);
    }

    /**
     * Check if getHeader returns empty string
     * when header doesn't have given key and
     * default value isn't given
     *
     * @return void
     */
    public function testGetHeaderReturnsEmptyStringWhenHeaderCannotBeFoundAndDefaultValueIsNull()
    {
        $result                     = $this->postRequest->getHeader($this->nonExistingHeader, null);

        $this->assertNotNull($result);
        $this->assertEmpty($result);
    }

    /**
     * Check if getHeader returns default value
     * when header doesn't have given key
     *
     * @return void
     */
    public function testGetHeaderReturnsGivenDataWhenHeaderCannotBeFound()
    {
        $result                     = $this->postRequest->getHeader($this->nonExistingHeader, $this->defaultData);

        $this->assertEquals($this->defaultData, $result);
    }

    /**
     * Check if the getFile returns the file array from
     * the $_FILES global when the file is available
     *
     * @return void
     */
    public function testGetFileReturnsFileArrayWhenFileCanBeFound()
    {
        $file = [
            $this->requestKey   => $this->defaultData
        ];

        $_FILES[$this->requestKey]  = $file;

        $result                     = $this->postRequest->getFile($this->requestKey);

        $this->assertEquals($file, $result);
    }

    /**
     * Check if the getFile returns an empty array
     * when the file is not available
     *
     * @return void
     */
    public function testGetFileReturnsEmptyArrayWhenFileCannotBeFound()
    {
        unset($_FILES);

        $result                     = $this->postRequest->getFile($this->requestKey);

        $this->assertEmpty($result);
    }
}
