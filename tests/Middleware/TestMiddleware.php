<?php declare(strict_types=1);

namespace RazorBit\API\Tests\Middleware;

use PHPUnit\Framework\TestCase;

use RazorBit\API\Contracts\IController;
use RazorBit\API\Contracts\IMiddleware;
use RazorBit\API\Contracts\IRequest;
use RazorBit\API\Contracts\IResponse;
use RazorBit\API\Responses\ErrorResponse;
use RazorBit\API\Responses\SuccessResponse;
use RazorBit\API\Tests\Middleware\MockMiddleware;

class TestMiddleware extends TestCase
{
    // Mocks
    private $nextMiddleware;
    private $controller;
    private $request;

    public function setUp()
    {
        // Setup mocks
        $this->nextMiddleware       = $this->createMock(IMiddleware::class);
        $this->controller           = $this->createMock(IController::class);
        $this->request              = $this->createMock(IRequest::class);
    }

    /**
     * Check if processRequest returns next middleware IResponse
     * when a nextMiddleware object is set
     *
     * @return void
     */
    public function testProcessRequestReturnsNextMiddlewareResponseWhenOneIsSet()
    {
        $this->nextMiddleware->method('processRequest')->willReturn(new SuccessResponse());
        $this->controller->method('processRequest')->willReturn(new ErrorResponse());

        $middleware = new MockMiddleware();
        $middleware->setNextMiddleware($this->nextMiddleware);

        $response = $middleware->processRequest($this->controller, $this->request);

        $this->assertInstanceOf(SuccessResponse::class, $response);
    }

    /**
     * Check if processRequest returns controllers IResponse
     * when no next middleware is set
     * 
     * @return void
     */
    public function testProcessRequestReturnsControllerResponseWhenNoNextMiddlewareIsSet()
    {
        $this->nextMiddleware->method('processRequest')->willReturn(new ErrorResponse());
        $this->controller->method('processRequest')->willReturn(new SuccessResponse());

        $middleware = new MockMiddleware();
        
        $response = $middleware->processRequest($this->controller, $this->request);

        $this->assertInstanceOf(SuccessResponse::class, $response);
    }
}
