<?php declare(strict_types=1);

namespace RazorBit\API\Tests\Middleware;

use RazorBit\API\Contracts\IController;
use RazorBit\API\Contracts\IRequest;
use RazorBit\API\Contracts\IResponse;
use RazorBit\API\Middleware\Middleware;

//public abstract function processRequest(IController $controller, IRequest $request): IResponse;

/**
 * Mock class to test the Middleware abstract class
 * 
 */
class MockMiddleware extends Middleware
{
    public function processRequest(IController $controller, IRequest $request): IResponse
    {
        return $this->next($controller, $request);       
    }
}
