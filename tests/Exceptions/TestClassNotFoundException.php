<?php declare(strict_types=1);

namespace RazorBit\API\Tests\Exceptions;

use PHPUnit\Framework\TestCase;

use RazorBit\API\Exceptions\ClassNotFoundException;

class TestClassNotFoundException extends TestCase
{
    // Test data
    private $exceptionMessage       = "MyException";

    // Test class
    private $classNotFoundException;

    public function setUp()
    {
        // Setup test class
        $this->classNotFoundException       = new ClassNotFoundException($this->exceptionMessage);
    }

    /**
     * Check if __toString returns class name with error message
     *
     * @return void
     */
    public function testToStringReturnsClassNameWithErrorMessage()
    {
        $expectedOutput                     = "[ClassNotFoundException]: $this->exceptionMessage";

        $result                             = (string)$this->classNotFoundException;

        $this->assertEquals($expectedOutput, $result);
    }
}
