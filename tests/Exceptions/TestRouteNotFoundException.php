<?php declare(strict_types=1);

namespace RazorBit\API\Tests\Exceptions;

use PHPUnit\Framework\TestCase;

use RazorBit\API\Exceptions\RouteNotFoundException;

class TestRouteNotFoundException extends TestCase
{
    // Test data
    private $exceptionMessage               = 'MyException';

    // Test class
    private $routeNotFoundException;

    public function setUp()
    {
        // Setup test class
        $this->routeNotFoundException       = new RouteNotFoundException($this->exceptionMessage);
    }

    /**
     * Check if __toString returns class name with error message
     *
     * @return void
     */
    public function testToStringReturnsClassNameWithErrorMessage()
    {
        $expectedOutput                     = "[RouteNotFoundException]: $this->exceptionMessage";

        $result                                 = (string)$this->routeNotFoundException;

        $this->assertEquals($expectedOutput, $result);
    }
}
