<?php declare(strict_types=1);

namespace RazorBit\API\Tests\Exceptions;

use PHPUnit\Framework\TestCase;

use RazorBit\API\Exceptions\DatabaseRecordNotFoundException;

class TestDatabaseRecordNotFoundException extends TestCase
{
    // Test data
    private $exceptionMessage                   = "MyException";

    // Test class
    private $databaseRecordNotFoundException;

    public function setUp()
    {
        // Setup test class
        $this->databaseRecordNotFoundException  = new DatabaseRecordNotFoundException($this->exceptionMessage);
    }

    /**
     * Check if __toString returns class name with error message
     *
     * @return void
     */
    public function testToStringReturnsClassNameWithErrorMessage()
    {
        $expectedOutput                         = "[DatabaseRecordNotFoundException]: $this->exceptionMessage";

        $result                                 = (string)$this->databaseRecordNotFoundException;

        $this->assertEquals($expectedOutput, $result);
    }
}
