<?php declare(strict_types=1);

namespace RazorBit\API\Tests\Exceptions;

use PHPUnit\Framework\TestCase;

use RazorBit\API\Exceptions\DatabaseException;

class TestDatabaseException extends TestCase
{
    // Test data
    private $exceptionMessage           = "MyException";

    // Test class
    private $databaseException;

    public function setUp()
    {
        // Setup test class
        $this->databaseException       = new DatabaseException($this->exceptionMessage);
    }

    /**
     * Check if __toString returns class name with error message
     *
     * @return void
     */
    public function testToStringReturnsClassNameWithErrorMessage()
    {
        $expectedOutput                 = "[DatabaseException]: $this->exceptionMessage";

        $result                         = (string)$this->databaseException;

        $this->assertEquals($expectedOutput, $result);
    }
}
