<?php declare(strict_types=1);

namespace RazorBit\API\Tests\Factories;

use PHPUnit\Framework\TestCase;

use RazorBit\API\Exceptions\ClassNotFoundException;
use RazorBit\API\Factories\RequestFactory;
use RazorBit\API\Requests\DeleteRequest;
use RazorBit\API\Requests\GetRequest;
use RazorBit\API\Requests\PostRequest;
use RazorBit\API\Requests\PutRequest;

class TestRequestFactory extends TestCase
{
    // Test Class
    private $requestFactory;

    public function setUp()
    {
        // Setup test class
        $this->requestFactory = new RequestFactory();
    }

    /**
     * Check if getRequest returns valid IRequest implementations
     * for each HTTP method
     *
     * @return void
     */
    public function testGetRequestReturnsValidRequestImplementationForValidMethod()
    {
        $deleteRequest  = $this->requestFactory->getRequest('DELETE');
        $getRequest     = $this->requestFactory->getRequest('GET');
        $postRequest    = $this->requestFactory->getRequest('POST');
        $putRequest     = $this->requestFactory->getRequest('PUT');

        $this->assertInstanceOf(DeleteRequest::class    , $deleteRequest);
        $this->assertInstanceOf(GetRequest::class       , $getRequest);
        $this->assertInstanceOf(PostRequest::class      , $postRequest);
        $this->assertInstanceOf(PutRequest::class       , $putRequest);
    }

    /**
     * Check if getRequest throws ClassNotFoundException
     * for invalid method
     *
     * @return void
     */
    public function testGetRequestThrowsExceptionForInvalidMethod()
    {
        $this->expectException(ClassNotFoundException::class);

        $this->requestFactory->getRequest('NON-EXISTENT');
    }
}
