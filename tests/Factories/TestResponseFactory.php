<?php declare(strict_types=1);

namespace RazorBit\API\Tests\Factories;

use PHPUnit\Framework\TestCase;

use RazorBit\API\Exceptions\ClassNotFoundException;
use RazorBit\API\Factories\ResponseFactory;
use RazorBit\API\Responses\BadRequestResponse;
use RazorBit\API\Responses\ErrorResponse;
use RazorBit\API\Responses\MethodNotAllowedResponse;
use RazorBit\API\Responses\NotFoundResponse;
use RazorBit\API\Responses\SuccessResponse;
use RazorBit\API\Responses\UnauthorizedResponse;

class TestResponseFactory extends TestCase
{
    // Test class
    private $responseFactory;

    public function setUp()
    {
        // Setup test class
        $this->responseFactory = new ResponseFactory();
    }

    /**
     * Check if getResponse returns valid IResponse implementations
     * for each HTTP method
     *
     * @return void
     */
    public function testGetResponseReturnsValidResponseImplementationForValidStatusCode()
    {
        $badRequestResponse         = $this->responseFactory->getResponse(400);
        $errorResponse              = $this->responseFactory->getResponse(500);
        $methodNotAllowedResponse   = $this->responseFactory->getResponse(405);
        $notFoundResponse           = $this->responseFactory->getResponse(404);
        $successResponse            = $this->responseFactory->getResponse(200);
        $unauthorizedResponse       = $this->responseFactory->getResponse(401);

        $this->assertInstanceOf(BadRequestResponse::class       , $badRequestResponse);
        $this->assertInstanceOf(ErrorResponse::class            , $errorResponse);
        $this->assertInstanceOf(MethodNotAllowedResponse::class , $methodNotAllowedResponse);
        $this->assertInstanceOf(NotFoundResponse::class         , $notFoundResponse);
        $this->assertInstanceOf(SuccessResponse::class          , $successResponse);
        $this->assertInstanceOf(UnauthorizedResponse::class     , $unauthorizedResponse);
    }

    /**
     * Check if getResponse throws ClassNotFoundException
     * for invalid status code
     *
     * @return void
     */
    public function testGetResponseThrowsExceptionForInvalidStatusCode()
    {
        $this->expectException(ClassNotFoundException::class);
        
        $this->responseFactory->getResponse(-2);
    }
}
