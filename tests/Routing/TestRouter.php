<?php declare(strict_types=1);

namespace RazorBit\API\Tests\Routing;

use PHPUnit\Framework\TestCase;

use RazorBit\API\Contracts\IRouteGroup;
use RazorBit\API\Contracts\IRequest;
use RazorBit\API\Contracts\IRequestFactory;
use RazorBit\API\Responses\BadRequestResponse;
use RazorBit\API\Responses\NotFoundResponse;
use RazorBit\API\Responses\SuccessResponse;
use RazorBit\API\Routing\Router;

class TestRouter extends TestCase
{
    // Test data
    private $existingRoute              = '/test';
    private $nonExistingRoute           = '/non/existing';

    // Mocks
    private $routeGroup1;
    private $routeGroup2;
    private $request;
    private $requestFactory;

    // Test class
    private $router;

    public function setUp()
    {
        // Setup mocks
        $this->routeGroup1              = $this->createMock(IRouteGroup::class);
        $this->routeGroup2              = $this->createMock(IRouteGroup::class);
        $this->request                  = $this->createMock(IRequest::class);
        $this->requestFactory           = $this->createMock(IRequestFactory::class);

        $this->routeGroup1->method('run')->willReturn(new SuccessResponse());
        $this->routeGroup2->method('run')->wilLReturn(new BadRequestResponse());
        $this->requestFactory->method('getRequest')->willReturn($this->request);

        // Setup test class
        $this->router                   = new Router($this->requestFactory);

        $this->router->registerGroup($this->routeGroup1);
        $this->router->registerGroup($this->routeGroup2);

        // Setup global variables
        $_SERVER['REQUEST_METHOD']      = 'POST';
    }

    /**
     * Check if run returns NotFound
     * when no registered route group has the
     * requested route
     *
     * @return void
     */
    public function testRunReturnsNotFoundForNonExistingRoute()
    {
        $result                         = $this->router->run($this->nonExistingRoute);

        $this->assertInstanceOf(NotFoundResponse::class, $result);
    }

    /**
     * Check if run returns SuccessResponse
     * when first route group gets called (first route group returns this response)
     *
     * @return void
     */
    public function testRunReturnsSuccessForFirstRegisteredRouteGroup()
    {
        $this->routeGroup1->method('hasRoute')->willReturn(true);
        $this->routeGroup2->method('hasRoute')->willReturn(false);

        $result                         = $this->router->run($this->existingRoute);

        $this->assertInstanceOf(SuccessResponse::class, $result);
    }

    /**
     * Check if run returns BadRequestResponse
     * when second route group gets called (second route group returns this response)
     *
     * @return void
     */
    public function testRunReturnsBadRequestForSecondRegisteredRouteGroup()
    {
        $this->routeGroup1->method('hasRoute')->willReturn(false);
        $this->routeGroup2->method('hasRoute')->willReturn(true);

        $result                         = $this->router->run($this->existingRoute);

        $this->assertInstanceOf(BadRequestResponse::class, $result);
    }
}
