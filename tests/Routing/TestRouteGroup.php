<?php declare(strict_types=1);

namespace RazorBit\API\Tests\Routing;

use PHPUnit\Framework\TestCase;

use RazorBit\API\Contracts\IMiddleware;
use RazorBit\API\Contracts\IRequest;
use RazorBit\API\Contracts\IResponse;
use RazorBit\API\Contracts\IRoute;
use RazorBit\API\Contracts\IRouteGroup;
use RazorBit\API\Exceptions\RouteNotFoundException;
use RazorBit\API\Responses\MethodNotAllowedResponse;
use RazorBit\API\Responses\NotFoundResponse;
use RazorBit\API\Responses\SuccessResponse;
use RazorBit\API\Responses\UnauthorizedResponse;
use RazorBit\API\Routing\RouteGroup;

class TestRouteGroup extends TestCase
{
    // Test data
    private $existingRoute      = '/test';
    private $nonExistingRoute   = '/non/existing';
    private $allowedMethods     = ['GET', 'POST'];
    private $nonAllowedMethod   = 'PUT';

    // Mocks
    private $route;
    private $middleware;
    private $request;

    // Test class
    private $routeGroup;

    public function setUp()
    {
        // Setup mocks
        $this->route            = $this->createMock(IRoute::class);
        $this->middleware       = $this->createMock(IMiddleware::class);
        $this->request          = $this->createMock(IRequest::class);

        $this->route->method('getUri')->willReturn($this->existingRoute);
        $this->route->method('getMethods')->willReturn($this->allowedMethods);

        // Setup test class
        $this->routeGroup       = new RouteGroup();

        $this->routeGroup->addRoute($this->route);
        $this->routeGroup->addMiddleware($this->middleware);
    }

    /**
     * Check if hasRoute returns true
     * for existing route
     *
     * @return void
     */
    public function testHasRouteReturnsTrueForExistingRoute()
    {
        $this->request->method('getType')->willReturn($this->allowedMethods[0]);
        $result                 = $this->routeGroup->hasRoute($this->existingRoute);

        $this->assertTrue($result);
    }

    /**
     * Check if hasRoute returns false
     * for non-existing route
     *
     * @return void
     */
    public function testHasRouteReturnsFalseForNonExistingRoute()
    {
        $this->request->method('getType')->willReturn($this->allowedMethods[0]);
        $result                 = $this->routeGroup->hasRoute($this->nonExistingRoute);

        $this->assertFalse($result);
    }

    /**
     * Check if run returns NotFoundResponse
     * for non-existing route
     *
     * @return void
     */
    public function testRunReturnsNotFoundForNonExistingRoute()
    {
        $this->request->method('getType')->willReturn($this->allowedMethods[0]);
        $this->middleware->method('processRequest')->willReturn(new SuccessResponse());

        $result                 = $this->routeGroup->run($this->nonExistingRoute, $this->request);

        $this->assertInstanceOf(NotFoundResponse::class, $result);
    }

    /**
     * Check if run returns SuccessResponse
     * for existing route
     *
     * @return void
     */
    public function testRunReturnsSuccessForExistingRoute()
    {
        $this->request->method('getType')->willReturn($this->allowedMethods[0]);
        $this->middleware->method('processRequest')->willReturn(new SuccessResponse());

        $result                 = $this->routeGroup->run($this->existingRoute, $this->request);

        $this->assertInstanceOf(SuccessResponse::class, $result);
    }

    /**
     * Check if run returns UnauthorizedResponse
     * for unauthorized user middleware
     *
     * @return void
     */
    public function testRunReturnsUnauthorizedForUnauthorizedMiddleware()
    {
        $this->request->method('getType')->willReturn($this->allowedMethods[0]);
        $this->middleware->method('processRequest')->willReturn(new UnauthorizedResponse());

        $result                 = $this->routeGroup->run($this->existingRoute, $this->request);

        $this->assertInstanceOf(UnauthorizedResponse::class, $result);
    }

    /**
     * Check if run returns MethodNotAllowedResponse
     * for non-allowed method in the route
     *
     * @return void
     */
    public function testRunReturnsMethodNotAllowedForNonAllowedMethod()
    {
        $this->request->method('getType')->willReturn($this->nonAllowedMethod);
        
        $result                 = $this->routeGroup->run($this->existingRoute, $this->request);

        $this->assertInstanceOf(MethodNotAllowedResponse::class, $result);
    }
}
