# Installing

__Note__: If you don't have a `composer.json` file please run `composer init` before continueing.

## Add repository

Locate your `composer.json` file and add the following repository to the `repositories` array:

```
{
    "type": "vcs",
    "url": "git@bitbucket.org:razorbitit/api.git"
}
```

## Install the package

Once the repository is added to you `composer.json` file you can just run `composer require razorbit/api:dev-master` and composer will take care of the rest.

# Testing

To test this package simply run `phpunit`

# Usage

## Get a Response instance

To get a response instance you can either directly instantiate an implementation `new SuccessResponse()` or keep
it abstracted away and using a factory. The response can be retrieved by passing the wanted HTTP status code.

Please keep in mind that non-supported status codes will return a `ErrorResponse` object.

```
$responseFactory = new RazorBit\API\Factories\ResponseFactory();
$response = $responseFactory->getResponse(200);
```

## Get a Request instance

To get a request instance you can either directly instantiate an implementation `new PostRequest()` or keep
it abstracted away using a factory. The request can be retrieved by passing the wanted HTTP method noun.

Please keep in mind that non-support HTTP method nouns will return `null`.

```
$requestFactory = new RazorBit\API\Factories\RequestFactory();
$request = $requestFactory->getRequest('POST');
```

## Create middleware

To create a new middleware object you just need to extend the `Middleware` class found in the `RazorBit\API\Middleware` namespace.

```
use RazorBit\API\Contracts\IController;
use RazorBit\API\Contracts\IRequest;
use RazorBit\API\Contracts\IResponse;
use RazorBit\API\Middleware\Middleware;

class CustomMiddleware extends Middleware
{
    public function processRequest(IController $controller, IRequest $request): IResponse
    {
        // TODO: Do something with the request

        return $this->next($controller, $request);
    }
}
```

## Create controller

To create a new controller object you just need to implement the `IController` interface found in the `RazorBit\API\Contracts` namespace.

```
use RazorBit\API\Contracts\IController;
use RazorBit\API\Contracts\IRequest;
use RazorBit\API\Contracts\IResponse;

class CustomController implements IController
{
    public function processRequest(IRequest $request): IResponse
    {
        // TODO: Do something with the request
    }
}
```

## Create database implementation

To add support for another database the selected DBMS must have PDO
support and it's driver must be installed on the server. The installation
of these drivers is out of scope for this document so the installation instructions must be fetched somewhere else.

When a PDO driver is available you need to implement the `IDatabase` interface found in the `RazorBit\API\Contracts` namespace.

```
use RazorBit\API\Contracts\IDatabase;

class MysqlDatabase implements IDatabase
{
    public function connect(): ?PDO
    {
        // Actually connect to the database
        // and return the PDO object
    }
}
```

## Create model

To create a new model simply implement the `IModel` inteface found in the `RazorBit\API\Contracts` namespace.

```
use RazorBit\API\Models\BaseModel;

class MockModel extends BaseModel
{
    public $id;
    public $username;
    public $password;

    protected $hiddenFields = [
        'password'
    ];
}

```

## Create endpoint

To create a new endpoint simply implement the `IEndpoint` interface found in the `RazorBit\API\Contracts` namespace.

```
use RazorBit\API\Contracts\IMiddleware;
use RazorBit\API\Contracts\IEndpoint;
use RazorBit\API\Contracts\IRequest;
use RazorBit\API\Contracts\IResponse;

class CustomEndpoint implements IEndpoint
{
    public function processRequest(IRequest $request): IResponse
    {
        // TODO: Build all the Dao's and the controller
    }
}
```

# Classes

## Available contracts

All the contracts can be found in the `RazorBit\API\Contracts` namespace.

- `IController`
- `IDatabase`
- `IMiddleware`
- `IModel`
- `IRequest`
- `IRequestFactory`
- `IResponse`
- `IResponseFactory`

## Available factories

All the factories can be found in the `RazorBit\API\Factories` namespace.

- `RequestFactory`
- `ResponseFactory`

## Available requests

All the requests can be found in the `RazorBit\API\Requests` namespace.

- `DeleteRequest`
- `GetRequest`
- `PostRequest`
- `PutRequest`

## Available responses

All the responses can be found in the `RazorBit\API\Responses` namespace.

- `SuccessResponse`
- `BadRequestResponse`
- `UnauthorizedResponse`
- `NotFoundResponse`
- `MethodNotAllowedResponse`
- `ErrorResponse`

# Traits

## Available validators

All the validators can be found in the `RazorBit\API\Validators` namespace.

- `GenericValidator`
