<?php declare(strict_types=1);

namespace RazorBit\API\Factories;

use RazorBit\API\Contracts\IResponseFactory;
use RazorBit\API\Contracts\IResponse;
use RazorBit\API\Exceptions\ClassNotFoundException;
use RazorBit\API\Responses\BadRequestResponse;
use RazorBit\API\Responses\ErrorResponse;
use RazorBit\API\Responses\MethodNotAllowedResponse;
use RazorBit\API\Responses\NotFoundResponse;
use RazorBit\API\Responses\SuccessResponse;
use RazorBit\API\Responses\UnauthorizedResponse;

class ResponseFactory implements IResponseFactory
{
    /**
     * Get Response implementation based on the
     * given statuscode
     *
     * @param integer $statusCode the status of the response
     *
     * @return IResponse
     */
    public function getResponse(int $statusCode): IResponse
    {
        switch ($statusCode)
        {
            case 200: return new SuccessResponse();
            case 400: return new BadRequestResponse();
            case 401: return new UnauthorizedResponse();
            case 404: return new NotFoundResponse();
            case 405: return new MethodNotAllowedResponse();
            case 500: return new ErrorResponse();
        }

        throw new ClassNotFoundException("get response for status code $statusCode");
    }
}
