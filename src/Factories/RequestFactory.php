<?php declare(strict_types=1);

namespace RazorBit\API\Factories;

use RazorBit\API\Contracts\IRequestFactory;
use RazorBit\API\Contracts\IRequest;
use RazorBit\API\Exceptions\ClassNotFoundException;
use RazorBit\API\Requests\DeleteRequest;
use RazorBit\API\Requests\GetRequest;
use RazorBit\API\Requests\PostRequest;
use RazorBit\API\Requests\PutRequest;

class RequestFactory implements IRequestFactory
{
    /**
     * Get the Request based on the given method name
     *
     * @param string $methodName the HTTP method name
     *
     * @return IRequest
     */
    public function getRequest(string $methodName): IRequest
    {
        switch (strtolower($methodName)) {
            case 'delete'   : return new DeleteRequest();
            case 'get'      : return new GetRequest();
            case 'post'     : return new PostRequest();
            case 'put'      : return new PutRequest();
        }

        throw new ClassNotFoundException("get request object for method $methodName");
    }
}
