<?php declare(strict_types=1);

namespace RazorBit\API\Responses;

use RazorBit\API\Contracts\IModel;

/**
 * class SuccessResponse
 * 
 * Response which should be returned when
 * the request is successfully handled.
 * Could be with a body but that is optional.
 */
class SuccessResponse extends Response
{
    /**
     * A generic status message
     *
     * @var string
     */
    private $statusMessage  = 'Success';

    /**
     * The status code for the response
     *
     * @var integer
     */
    private $statusCode = 200;

    /**
     * The body that will be displayed
     *
     * @var array
     */
    private $body;

    /**
     * Allow optional body parameter to
     * be given when the SuccessResponse is
     * initialized
     *
     * @param IModel $model
     */
    public function __construct(IModel $model = null)
    {
        if ($model != null) {
            $this->setBody($model->toArray());
        }
    }

    /**
     * Get the status code for the response
     *
     * @return integer the status code
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * Display an object or array as an array
     *
     * @return array an object or array in array format
     */
    protected function getBody(): array
    {
        if ($this->body !== null) {
            return $this->body;
        }

        return [
            'message' => $this->statusMessage
        ];
    }

    /**
     * Set the body element
     *
     * @param array $body the body which'll be displayed
     * @return void
     */
    public function setBody(array $body)
    {
        $this->body = $body;
    }
}