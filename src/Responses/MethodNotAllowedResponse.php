<?php declare(strict_types=1);

namespace RazorBit\API\Responses;

/**
 * class MethodNotAllowedResponse
 * 
 * Response which should be returned when
 * a user sends an incorrect HTTP request to
 * an endpoint.
 */
class MethodNotAllowedResponse extends Response
{
    /**
     * A generic status message
     *
     * @var string
     */
    private $statusMessage  = 'Method Not Allowed';

    /**
     * The status code for the response
     *
     * @var integer
     */
    private $statusCode     = 405;

    /**
     * Get the status code for the response
     *
     * @return integer the status code
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * Show a generic message field
     *
     * @return array generic message field
     */
    protected function getBody(): array
    {
        return [
            'message' => $this->statusMessage
        ];
    }
}