<?php declare(strict_types=1);

namespace RazorBit\API\Responses;

/**
 * class BadRequestResponse
 * 
 * Response which should be returned when
 * a user submits invalid or no fields to
 * API.
 */
class BadRequestResponse extends Response
{
    /**
     * A generic status message
     *
     * @var string
     */
    private $statusMessage  = 'Bad Request';

    /**
     * The status code for the response
     *
     * @var integer
     */
    private $statusCode     = 400;

    /**
     * The invalid fields with a message to display
     * alongside the fieldname
     *
     * @var array
     */
    private $invalidFields;

    /**
     * Allow optional invalid fields parameter to
     * be given when the BadRequestResponse is
     * initialized
     *
     * @param array $invalidFields
     */
    public function __construct(array $invalidFields = null)
    {
        if ($invalidFields != null) {
            $this->addInvalidFields($invalidFields);
        }
    }

    /**
     * Get the status code for the response
     *
     * @return integer the status code
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * If there are missing fields display these in an
     * array otherwise show a generic message field
     *
     * @return array missing fields or a generic message field
     */
    protected function getBody(): array
    {
        if ($this->invalidFields != null && count($this->invalidFields) > 0) {
            return $this->invalidFields;
        }

        return [
            'message' => $this->statusMessage
        ];
    }

    /**
     * Add multiple invalid fields
     *
     * @param array $fields the fields that are incorrect
     * 
     * @return void
     */
    public function addInvalidFields(array $fields)
    {
        foreach ($fields as $fieldName => $message) {
            $this->addInvalidField($fieldName, $message);
        }
    }

    /**
     * Add an invalid field with a message that'll be
     * displayed in JSON format 
     *
     * @param string $fieldName the field that's incorrect
     * @param string $message a message that'll be displayed for the field
     * 
     * @return void
     */
    private function addInvalidField(string $fieldName, string $message)
    {
        $this->invalidFields[$fieldName] = $message;
    }
}