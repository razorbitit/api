<?php declare(strict_types=1);

namespace RazorBit\API\Responses;

/**
 * class UnauthorizedResponse
 * 
 * Response which should be returned when
 * a user isn't signed in or has insufficient
 * rights.
 */
class UnauthorizedResponse extends Response
{
    /**
     * A generic status message
     *
     * @var string
     */
    private $statusMessage  = 'Unauthorized';

    /**
     * The status code for the response
     *
     * @var integer
     */
    private $statusCode     = 401;

    /**
     * Get the status code for the response
     *
     * @return integer the status code
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * Show a generic message field
     *
     * @return array generic message field
     */
    protected function getBody(): array
    {
        return [
            'message' => $this->statusMessage
        ];
    }
}