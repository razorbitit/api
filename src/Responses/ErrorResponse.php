<?php declare(strict_types=1);

namespace RazorBit\API\Responses;

/**
 * class ErrorResponse
 * 
 * Response which should be returned when
 * an unexpected or otherwise unknown error
 * occurred.
 */
class ErrorResponse extends Response
{
    /**
     * A generic status message
     *
     * @var string
     */
    private $statusMessage  = 'Internal Server Error';

    /**
     * The status code for the response
     *
     * @var integer
     */
    private $statusCode     = 500;

    /**
     * Get the status code for the response
     *
     * @return integer the status code
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * Show a generic message field
     *
     * @return array generic message field
     */
    protected function getBody(): array
    {
        return [
            'message' => $this->statusMessage
        ];
    }
}