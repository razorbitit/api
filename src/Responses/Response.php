<?php declare(strict_types=1);

namespace RazorBit\API\Responses;

use RazorBit\API\Contracts\IResponse;

/**
 * abstract class Response
 * 
 * Implements the basic Response interface methods
 * that all response implementations have in common
 */
abstract class Response implements IResponse
{
    /**
     * Set the status code and content-type headers
     * for the currenct response if the headers aren't
     * already sent
     * 
     * Display the body of the implementation as a JSON 
     * object
     *
     * @return void
     */
    public function display()
    {
        if (!headers_sent()) {
            http_response_code($this->getStatusCode());
            header('Content-Type: application/json');
        }

        echo json_encode($this->getBody());
    }

    protected abstract function getBody(): array;
}