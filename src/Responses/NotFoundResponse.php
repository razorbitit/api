<?php declare(strict_types=1);

namespace RazorBit\API\Responses;

/**
 * class NotFoundResponse
 * 
 * Response which should be returned when
 * an user tries to retrieve a resource that
 * doesn't exist (anymore).
 */
class NotFoundResponse extends Response
{
    /**
     * A generic status message
     *
     * @var string
     */
    private $statusMessage  = 'Not Found';

    /**
     * The status code for the response
     *
     * @var integer
     */
    private $statusCode     = 404;

    /**
     * Get the status code for the response
     *
     * @return integer the status code
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * Show a generic message field
     *
     * @return array generic message field
     */
    protected function getBody(): array
    {
        return [
            'message' => $this->statusMessage
        ];
    }
}