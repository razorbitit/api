<?php declare(strict_types=1);

namespace RazorBit\API\Validators;

use Respect\Validation\Validator as V;

Trait GenericValidator
{
    /**
     * Validate the given e-mail address
     *
     * @param string $emailAddress
     *
     * @return boolean true => valid email address, false => invalid email address
     */
    public function validateEmailAddress(string $emailAddress): bool
    {
        return V::email()->validate($emailAddress);
    }

    /**
     * Validate the given phone number
     *
     * @param string $phoneNumber
     *
     * @return boolean true => valid phone number, false => invalid phone number
     */
    public function validatePhoneNumber(string $phoneNumber): bool
    {
        $phoneNumber = preg_replace('/\s+/', '', $phoneNumber);

        return V::phone()->validate($phoneNumber);
    }

    /**
     * Validate that two string are equal
     *
     * @param string $a
     * @param string $b
     *
     * @return boolean true => strings are equal, false => strings are not equal
     */
    public function validateEquals(string $a, string $b): bool
    {
        return $a == $b;
    }

    /**
     * Validate that the given value size is larger or equal
     * to the given minLength
     *
     * @param string $value
     * @param integer $minLength
     *
     * @return boolean true => value size is larger or equal to given minLength, false => value size is less than given minLength
     */
    public function validateMinLength(string $value = null, int $minLength = 0): bool
    {
        if ($value == null) {
            return false;
        }

        return strlen($value) >= $minLength;
    }

    /**
     * Validate that the given value size is smaller or equal
     * to the given maxLength
     *
     * @param string $value
     * @param integer $maxLength
     *
     * @return boolean true => value size is less or equal to the given maxLength, false => the value size is larger than the given maxLength
     */
    public function validateMaxLength(string $value = null, int $maxLength = 0): bool
    {
        if ($value == null) {
            return false;
        }

        return strlen($value) <= $maxLength;
    }

    /**
     * Validate that the given value is an integer
     * 
     * @param mixed $value
     * 
     * @return bool true => The value is an integer, false => the value isn't an integer
     */
    public function validateInteger($value): bool
    {
        return (
                is_integer($value)      ||
                ctype_digit($value)
        );
    }

    /**
     * Validate that the given date is in the given format
     * 
     * @param mixed $date
     * @param string $dateFormat
     * 
     * @return bool true => The date is in the given format, false => the given date is in another format
     */
    public function validateDateFormat($date, string $dateFormat): bool
    {
        $dateTime = \DateTime::createFromFormat($dateFormat, $date);
        
        return (
                $dateTime           && 
                $dateTime->format($dateFormat) === $date
        );
    }

    /**
     * Validate that the value is not null
     * 
     * @param mixed $value
     * @return bool true => The value is NOT null, false => the value is null
     */
    public function validateNonNull($value): bool
    {
        return (
            $this->validateInteger($value) ||
            $value != null
        );
    }
}
