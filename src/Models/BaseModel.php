<?php declare(strict_types=1);

namespace RazorBit\API\Models;

use RazorBit\API\Contracts\IModel;

abstract class BaseModel implements IModel
{
    /**
     * Turn object into JSON object.
     * 
     * Hide the hiddenFields as given in the
     * implementation, because we do this from
     * the context of the object itself the 
     * get_object_vars method also returns
     * the protected members so we explicitly
     * hide the 'hiddenFields' member
     *
     * @return array representation of the model
     */
    public function toArray(): array
    {
        $array          = array();
        $hiddenFields   = array();

        if (isset($this->hiddenFields)) {
            $hiddenFields   = $this->hiddenFields;
        }

        foreach ($this as $key => $value) {
            if (!in_array($key, $hiddenFields)) {
                if (is_a($value, IModel::class)) {
                    $array[$key]    = $value->toArray();
                } else {
                    $array[$key]    = $value;
                }
            }
        }

        // Explicitly remove the hiddenFields key from the array
        unset($array['hiddenFields']);

        return $array;
    }
}
