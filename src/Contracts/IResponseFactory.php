<?php declare(strict_types=1);

namespace RazorBit\API\Contracts;

use RazorBit\API\Contracts\IResponse;

interface IResponseFactory
{
    public function getResponse(int $statusCode): IResponse;
}