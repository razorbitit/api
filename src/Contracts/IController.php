<?php declare(strict_types=1);

namespace RazorBit\API\Contracts;

use RazorBit\API\Contracts\IRequest;
use RazorBit\API\Contracts\IResponse;

interface IController
{
    public function processRequest(IRequest $request): IResponse;
}
