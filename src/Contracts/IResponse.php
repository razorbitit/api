<?php declare(strict_types=1);

namespace RazorBit\API\Contracts;

/**
 * interface IResponse
 * 
 * Basic methods the API can expect
 * for a Response implementation.
 */
interface IResponse
{
    public function display();
    public function getStatusCode(): int;
}