<?php declare(strict_types=1);

namespace RazorBit\API\Contracts;

/**
 * interface IRequest
 *
 * Basic methods the API can expect
 * for a Request implementation.
 */
interface IRequest
{
    public function getHeader(string $name, string $defaultValue = ''): string;
    public function hasValue(string $key): bool;
    public function getType(): string;
    public function getValue(string $key, string $defaultValue = ''): string;
    public function getIntegerValue(string $key, int $defaultValue = -1): int;
    public function getFile(string $key): array;
    public function getRawBody(): string;
}
