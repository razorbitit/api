<?php declare(strict_types=1);

namespace RazorBit\API\Contracts;

use RazorBit\API\Contracts\IRequest;

interface IRequestFactory
{
    public function getRequest(string $methodName): IRequest;
}
