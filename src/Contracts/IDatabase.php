<?php declare(strict_types=1);

namespace RazorBit\API\Contracts;

use PDO;

/**
 * Interface IDatabase
 *
 * Basic methods the API can expect
 * for a Database implementation
 */
interface IDatabase
{
    public function createNewInstance(): IDatabase;
    public function connect(): PDO;
    public function executeQuery(string $sqlQuery, array $arguments): bool;
    public function fetch();
    public function fetchAll(): array;
    public function getRecordCount(): int;
    public function getLastInsertedId(string $name = ''): int;
    public function close();
}
