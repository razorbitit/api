<?php declare(strict_types=1);

namespace RazorBit\API\Contracts;

/**
 * interface IModel
 * 
 * Basic conversion methods which'll
 * be needed by various methods
 * 
 */
interface IModel
{
    public function toArray(): array;
}
