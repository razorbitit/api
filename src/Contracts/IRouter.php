<?php declare(strict_types=1);

namespace RazorBit\API\Contracts;

use RazorBit\API\Contracts\IRouteGroup;
use RazorBit\API\Contracts\IResponse;

interface IRouter
{
    public function registerGroup(IRouteGroup ...$groups);
    public function run(string $route): IResponse;
}
