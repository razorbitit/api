<?php declare(strict_types=1);

namespace RazorBit\API\Contracts;

use RazorBit\API\Contracts\IController;

interface IRoute
{
    public function getMethods(): array;
    public function getUri(): string;
    public function getController(): IController;
}
