<?php

namespace RazorBit\API\Contracts;

use RazorBit\API\Contracts\IMiddleware;
use RazorBit\API\Contracts\IRequest;
use RazorBit\API\Contracts\IResponse;
use RazorBit\API\Contracts\IRoute;

interface IRouteGroup
{
    public function addRoute(IRoute ...$routes): IRouteGroup;
    public function addMiddleware(IMiddleware ...$middleware): IRouteGroup;
    public function hasRoute(string $route): bool;
    public function run(string $route, IRequest $request): IResponse;
}
