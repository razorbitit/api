<?php declare(strict_types=1);

namespace RazorBit\API\Contracts;

use RazorBit\API\Contracts\IController;
use RazorBit\API\Contracts\IRequests;
use RazorBit\API\Contracts\IResponse;

/**
 * Interface IMiddleware
 * 
 * Basic methods the API can expect
 * for a Middleware implementation.
 */
interface IMiddleware
{
    public function setNextMiddleware(IMiddleware $middleware);
    public function processRequest(IController $controller, IRequest $request): IResponse;
}