<?php declare(strict_types=1);

namespace RazorBit\API\Middleware;

use RazorBit\API\Contracts\IController;
use RazorBit\API\Contracts\IRequest;
use RazorBit\API\Contracts\IResponse;
use RazorBit\API\Middleware\Middleware;

/**
 * class EmptyMiddleware
 * 
 * This middleware class gets used when a
 * null could be returned
 */
class EmptyMiddleware extends Middleware
{
    /**
     * Directly pass the request to the controller
     *
     * @param IController $controller
     * @param IRequest $request
     *
     * @return IResponse
     */
    public function processRequest(IController $controller, IRequest $request): IResponse
    {
        return $controller->processRequest($request);
    }
}
