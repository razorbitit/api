<?php declare(strict_types=1);

namespace RazorBit\API\Middleware;

use RazorBit\API\Contracts\IController;
use RazorBit\API\Contracts\IMiddleware;
use RazorBit\API\Contracts\IRequest;
use RazorBit\API\Contracts\IResponse;

/**
 * abstract class Middleware
 * 
 * Implements the basic Middleware interface methods
 * that all middleware implementations have in common
 */
abstract class Middleware implements IMiddleware
{
    /**
     * The next middleware that will be processed
     * when the current middleware succeeds
     *
     * @var IMiddleware
     */
    private $nextMiddleware;

    /**
     * Add another middleware layer to the current
     * middleware object
     * 
     * If there is a next middleware object the given
     * middleware will be passed to the setNextMiddleware
     * method on the current nextMiddleware. Thus making
     * sure the next middleware will always be executed
     *
     * @param IMiddleware $middleware the next middleware to process
     * @return void
     */
    public function setNextMiddleware(IMiddleware $middleware)
    {
        if ($this->nextMiddleware == null) {
            $this->nextMiddleware = $middleware;
        } else {
            $this->nextMiddleware->setNextMiddleware($middleware);
        }
    }

    /**
     * Validate the request against the nextMiddleware or run the
     * controller if the nextMiddleware is null
     * 
     * This method should be called from the processRequest
     * method implementation
     *
     * @param Icontroller $controller the controller which should handle the request
     * @param IRequest $request the request that's being sent to the API
     * @return IResponse the response the middleware or the controller will return
     */
    protected function next(Icontroller $controller, IRequest $request): IResponse
    {
        if ($this->nextMiddleware == null) {
            return $controller->processRequest($request);
        }

        return $this->nextMiddleware->processRequest($controller, $request);
    }

    public abstract function processRequest(IController $controller, IRequest $request): IResponse;
}