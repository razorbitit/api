<?php declare(strict_types=1);

namespace RazorBit\API\Requests;

class GetRequest extends Request
{
    /**
     * Read the value from the given key from the
     * GET array
     *
     * @param string $key the key for which to get the value
     * @param string $defaultValue the value that's returned when the key does not exist
     * @return string the value for the key or the given default value
     */
    public function getValue(string $key, string $defaultValue = null): string
    {
        if (isset($_GET[$key])) {
            return trim($_GET[$key]);
        }

        return isset($defaultValue) ? $defaultValue : '';
    }
}