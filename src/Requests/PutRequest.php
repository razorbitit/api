<?php declare(strict_types=1);

namespace RazorBit\API\Requests;

class PutRequest extends Request
{
    /**
     * Read the value from the given key from the
     * php://input 'file'
     *
     * @param string $key the key for which to get the value
     * @param string $defaultValue the value that's returned when the key does not exist
     * @return string the value for the key or the given default value
     */
    public function getValue(string $key, string $defaultValue = null): string
    {
        $data       = file_get_contents('php://input');
        $putData    = array();

        parse_str($data, $putData);

        if (isset($putData[$key])) {
            return trim($putData[$key]);
        }

        return isset($defaultValue) ? $defaultValue : '';
    }
}