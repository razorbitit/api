<?php declare(strict_types=1);

namespace RazorBit\API\Requests;

use RazorBit\API\Contracts\IRequest;
use RazorBit\API\Exceptions\InvalidDataException;

/**
 * abstract class Request
 *
 * Implements the basic Request interface methods
 * that all request implementations have in common
 */
abstract class Request implements IRequest
{
    public static $DELETE   = 'DELETE';
    public static $GET      = 'GET';
    public static $POST     = 'POST';
    public static $PUT      = 'PUT';

    /**
     * Read the header from the request based on the
     * given name
     *
     * @param string $name the name of the header element exc. HTTP_ prefix
     * @param string $defaultValue the default value if the header element could not be found
     * @return string the value of the header
     */
    public function getHeader(string $name, string $defaultValue = null): string
    {
        $headerKey  = strtoupper($name);

        if (isset($_SERVER["HTTP_$headerKey"])) {
            return $_SERVER["HTTP_$headerKey"];
        }

        return isset($defaultValue) ? $defaultValue : '';
    }

    /**
     * Get the value of the request as an integer
     *
     * If a default value is given that value is returned
     * when the value isn't an integer otherwise an exception
     * is thrown
     *
     * @param string $key the key to look for
     * @param int $defaultValue [= -9] the default value when the keys doesn't have a value
     * @return integer the value of the key as an integer or the default value
     * if the value isn't an integer
     */
    public function getIntegerValue(string $key, int $defaultValue = -1): int
    {
        $value      = $this->getValue($key);
        $isInteger  = (is_integer($value) || ctype_digit($value));

        if (!$isInteger) {
            return $defaultValue;
        }

        return intval($value);
    }

    /**
     * Check if the request has the value for the given
     * key
     *
     * @param string $key the key to look for
     * @return boolean wheter or not the value for the key exists
     */
    public function hasValue(string $key): bool
    {
        return strlen($this->getValue($key)) > 0;
    }

    /**
     * Get the HTTP method for the request
     *
     * @return string
     */
    public function getType(): string
    {
        return strtoupper($_SERVER['REQUEST_METHOD']);
    }

    /**
     * Get the FILE array for the given key
     * returns an empty array when no file could
     * be found
     *
     * @param string $key
     *
     * @return array
     */
    public function getFile(string $key): array
    {
        $file           = array();

        if (isset($_FILES[$key])) {
            $file       = $_FILES[$key];
        }

        return $file;
    }

    /**
     * Get the raw HTTP body from the request
     *
     * @return string
     */
    public function getRawBody(): string
    {
        return file_get_contents('php://input');
    }

    public abstract function getValue(string $key, string $defaultValue = ''): string;
}
