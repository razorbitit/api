<?php declare(strict_types=1);

namespace RazorBit\API\Routing;

use RazorBit\API\Contracts\IMiddleware;
use RazorBit\API\Contracts\IRequest;
use RazorBit\API\Contracts\IResponse;
use RazorBit\API\Contracts\IRoute;
use RazorBit\API\Contracts\IRouteGroup;
use RazorBit\API\Exceptions\RouteNotFoundException;
use RazorBit\API\Responses\MethodNotAllowedResponse;
use RazorBit\API\Responses\NotFoundResponse;
use RazorBit\API\Middleware\EmptyMiddleware;

class RouteGroup implements IRouteGroup
{
    /**
     * Array of IRoute objects
     *
     * @var array
     */
    private $routes         = array();

    /**
     * Array of IMiddleware objects
     *
     * @var array
     */
    private $middleware     = array();

    /**
     * Add one or more routes
     *
     * @param Route ...$routes
     *
     * @return void
     */
    public function addRoute(IRoute ...$routes): IRouteGroup
    {
        $this->routes = array_merge($this->routes, $routes);

        return $this;
    }

    /**
     * Add one or more middleware instances
     *
     * @param IMiddleware ...$middleware
     *
     * @return void
     */
    public function addMiddleware(IMiddleware ...$middleware): IRouteGroup
    {
        $this->middleware = array_merge($this->middleware, $middleware);

        return $this;
    }

    /**
     * Check if group has a certain route
     *
     * @param string $route
     *
     * @return boolean
     */
    public function hasRoute(string $route): bool
    {
        try {
            $this->getRoute($route);

            return true;
        } catch(RouteNotFoundException $e) {
            return false;
        }
    }

    /**
     * Execute the request and pass it through
     * the middleware (if available) or just
     * run the controller
     *
     * @param string $route
     * @param IRequest $request
     *
     * @return IResponse
     */
    public function run(string $uri, IRequest $request): IResponse
    {
        // Check if route exists
        if (!$this->hasRoute($uri)) {
            return new NotFoundResponse();
        }
        
        $route              = $this->getRoute($uri);
        $controller         = $route->getController();
        $middleware         = $this->getMiddleware();

        // Check if request method is allowed for the route
        if (!in_array($request->getType(), $route->getMethods())) {
            return new MethodNotAllowedResponse();
        }

        // Check if the route group has middleware
        if ($middleware != null) {
            return $middleware->processRequest($controller, $request);
        }

        return $controller->processRequest($request);
    }

    /**
     * Put all middleware objects from array into
     * single middleware object
     *
     * @return IMiddleware
     */
    private function getMiddleware(): IMiddleware
    {
        $middleware             = new EmptyMiddleware();

        // Get first middleware object
        if (isset($this->middleware) && isset($this->middleware[0])) {
            $middleware         = $this->middleware[0];
        }
        
        // Chain all middleware objects to first middleware object
        for ($i = 1; $i < count($this->middleware); $i++) {
            $middleware->setNextMiddleware($this->middleware[$i]);
        }

        return $middleware;
    }

    /**
     * Get route object based on the route
     * string
     *
     * @param string $route
     *
     * @return IRoute
     */
    private function getRoute(string $route): IRoute
    {
        foreach ($this->routes as $routeObject) {
            if ($routeObject->getUri() == $route) {
                return $routeObject;
            }
        }

        throw new RouteNotFoundException($route);
    }
}
