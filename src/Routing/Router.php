<?php declare(strict_types=1);

namespace RazorBit\API\Routing;

use RazorBit\API\Contracts\IMiddleware;
use RazorBit\API\Contracts\IRequest;
use RazorBit\API\Contracts\IRequestFactory;
use RazorBit\API\Contracts\IRouteGroup;
use RazorBit\API\Contracts\IRouter;
use RazorBit\API\Contracts\IResponse;
use RazorBit\API\Responses\NotFoundResponse;
use RazorBit\API\Responses\SuccessResponse;

class Router implements IRouter
{
    /**
     * Factory for getting the request object
     * based on the HTTP verb
     *
     * @var IRequestFactory
     */
    private $requestFactory;

    /**
     * Registered route groups
     *
     * @var array
     */
    private $routeGroups        = array();

    /**
     * Inject dependencies
     *
     * @param IRequestFactory $requestFactory
     */
    public function __construct(IRequestFactory $requestFactory)
    {
        $this->requestFactory   = $requestFactory;
    }

    /**
     * Add one or more groups to the router groups
     *
     * @param IRouteGroup ...$groups
     *
     * @return void
     */
    public function registerGroup(IRouteGroup ...$groups)
    {
        $this->routeGroups = array_merge($this->routeGroups, $groups);
    }

    /**
     * Run the requested route if one of
     * the registered groups has it, otherwise
     * return not found
     *
     * @param string $route
     *
     * @return IResponse
     */
    public function run(string $route): IResponse
    {
        $request                = $this->getRequest();

        foreach ($this->routeGroups as $group) {
            if ($group->hasRoute($route)) {
                return $group->run($route, $request);
            }
        }

        return new NotFoundResponse();
    }

    /**
     * Get request object based on the HTTP verb
     *
     * @return IRequest
     */
    private function getRequest(): IRequest
    {
        $requestVerb        = $_SERVER['REQUEST_METHOD'];
        $request            = $this->requestFactory->getRequest($requestVerb);

        return $request;
    }
}
