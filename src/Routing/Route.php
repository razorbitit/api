<?php declare(strict_types=1);

namespace RazorBit\API\Routing;

use RazorBit\API\Contracts\IController;
use RazorBit\API\Contracts\IRoute;

class Route implements IRoute
{
    /**
     * HTTP Method VERBS
     *
     * @var array
     */
    private $methods;

    /**
     * Asset URI
     *
     * @var string
     */
    private $uri;

    /**
     * Controller to handle the route
     *
     * @var IController
     */
    private $controller;

    /**
     * Build the Route object
     *
     * @param string $method
     * @param string $uri
     * @param IController $controller
     */
    public function __construct(array $methods, string $uri, IController $controller)
    {
        $this->methods      = $methods;
        $this->uri          = $uri;
        $this->controller   = $controller;
    }

    public function getMethods(): array
    {
        return $this->methods;
    }

    public function getUri(): string
    {
        return $this->uri;
    }

    public function getController(): IController
    {
        return $this->controller;
    }
}
