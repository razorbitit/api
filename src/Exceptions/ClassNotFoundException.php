<?php declare(strict_types=1);

namespace RazorBit\API\Exceptions;

class ClassNotFoundException extends \Exception
{
    public function __construct(string $message, int $code = 0, Exception $previousException = null)
    {
        parent::__construct($message, $code, $previousException);
    }

    /**
     * Get class name with the given error message
     *
     * @return string
     */
    public function __toString(): string
    {
        $className          = (new \ReflectionClass($this))->getShortName();
        return "[$className]: $this->message";
    }
}
